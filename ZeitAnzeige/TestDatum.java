public class TestDatum {
    public TestDatum() {
        testeSchaltjahr();
    }
    
    public void testeSchaltjahr() {
        DatumsAnzeige ja = new DatumsAnzeige(2012);
        DatumsAnzeige ma = new DatumsAnzeige(12);
        TagesAnzeige  ta = new TagesAnzeige(ma, ja);
        
        System.out.println( ta.ist_schaltjahr(1972) );
        System.out.println( ta.ist_schaltjahr(1900) );
        System.out.println( ta.ist_schaltjahr(1903) );
        System.out.println( ta.ist_schaltjahr(2000) );
        System.out.println( ta.ist_schaltjahr(2012) );
    }
    
}
