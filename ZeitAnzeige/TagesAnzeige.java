public class TagesAnzeige {
    private DatumsAnzeige monatsAnzeige;
    private DatumsAnzeige jahresAnzeige;
    private int wert;

    public TagesAnzeige(DatumsAnzeige neueMonatsAnzeige, DatumsAnzeige neueJahresAnzeige) {
        wert  = 1;
        monatsAnzeige = neueMonatsAnzeige;
        jahresAnzeige = neueJahresAnzeige;
    }

    public boolean tick() {
        int limit;
        int monat = monatsAnzeige.gibWert();
        System.out.println(monat);

        if (monat == 2) {
            if ( ist_schaltjahr( jahresAnzeige.gibWert() )) {
                limit = 29;
            } else {
                limit = 28;
            }
        } else if (monat == 1 || monat == 3 || 
        monat == 5 || monat == 7 || monat == 8 ||
        monat == 10 || monat == 12) {
            limit = 31;
        } else {
            limit = 30;
        }

        // >= weggemacht, da Limit hier der
        // größtmögliche gültige Wert ist
        if (wert + 1 > limit) {
            // Wenn Limit überschritten
            wert = 1;
            return true;
        } else {
            // Wert weiterzaehlen
            wert = wert + 1;
            return false;
        }
    }

    public String toString() {
        String string;

        if (wert <= 9) {
            string = "0" + wert;
        } else {
            string = "" + wert;
        }

        return string;
    }

    public void setzeWert(int neuerWert) {
        if (neuerWert >= 31) {
            System.out.println("Fehler: " + neuerWert + " zu groß!");
            // Programm muss abgebrochen werden.
        } else {
            wert = neuerWert;
        }
    }

    public boolean ist_schaltjahr(int jahr) {
        if (jahr % 400 == 0) {
            return true;
        } else if (jahr % 100 == 0) {
            return false;
        } else if (jahr % 4 == 0) {
            return true;
        } else  {
            return false;
        }
    }

}
