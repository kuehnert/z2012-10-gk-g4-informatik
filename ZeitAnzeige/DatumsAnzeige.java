public class DatumsAnzeige {
  private int wert;
  private int limit;

  public DatumsAnzeige(int neuesLimit) {
      wert  = 1;
      limit = neuesLimit;
  }
    
  public boolean tick() {
      if (wert + 1 >= limit) {
          // Wenn Limit überschritten
          wert = 1;
          return true;
      } else {
          // Wert weiterzaehlen
          wert = wert + 1;
          return false;
      }
  }
    
  public String toString() {
      String string;
      
      if (limit > 999) {
        if (wert <= 9) {
            string = "000" + wert;
        } else if (wert <= 99) {
            string = "00" + wert;
        } else if (wert <= 999) {
            string = "0" + wert;
        } else {
            string = "" + wert;
        }
      } else {
        if (wert <= 9) {
            string = "0" + wert;
        } else {
            string = "" + wert;
        }
      }
      
        
      return string;
  }
    
  public void setzeWert(int neuerWert) {
      if (neuerWert >= limit) {
          System.out.println("Fehler: " + neuerWert + " zu groß!");
          // Programm muss abgebrochen werden.
      } else {
          wert = neuerWert;
      }
  }
  
  public int gibWert() {
    return wert;
  }
}
