public class KalenderSteuerung {
  private DatumsAnzeige jahresAnzeige;    
  private DatumsAnzeige monatsAnzeige;    
  private TagesAnzeige  tagesAnzeige;
  private String        datumsAusgabe;

  public KalenderSteuerung() {
      jahresAnzeige  = new DatumsAnzeige(9999);
      monatsAnzeige  = new DatumsAnzeige(13);
      tagesAnzeige   = new TagesAnzeige(monatsAnzeige, jahresAnzeige);
      datumFormatieren();
  }

  public void datumFormatieren() {
      // Sammel die drei Strings der Anzeigen zusammen
      // und trenne sie mit Doppelpunkt;
      datumsAusgabe = "";
      datumsAusgabe = datumsAusgabe + tagesAnzeige.toString();
      datumsAusgabe = datumsAusgabe + "." + monatsAnzeige.toString();
      datumsAusgabe = datumsAusgabe + "." + jahresAnzeige.toString();
  }
    
  public void tick() {
      boolean ueberlauf;

      ueberlauf = tagesAnzeige.tick();
      if (ueberlauf) {
          ueberlauf = monatsAnzeige.tick();
          if (ueberlauf) {
              jahresAnzeige.tick();
          }
      }

      datumFormatieren();
  }
    
  public void setzeZeit(int neuesJahr, int neuerMonat, int neuerTag) {
      tagesAnzeige.setzeWert(neuerTag);
      monatsAnzeige.setzeWert(neuerMonat);
      jahresAnzeige.setzeWert(neuesJahr);
      datumFormatieren();
  }
}
