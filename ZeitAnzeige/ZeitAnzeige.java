public class ZeitAnzeige {
    private int wert;
    private int limit;

    public ZeitAnzeige(int neuesLimit) {
        wert  = 0;
        limit = neuesLimit;
    }
    
    public boolean tick() {
        if (wert + 1 >= limit) {
            // Wenn Limit überschritten
            wert = 0;
            return true;
        } else {
            // Wert weiterzaehlen
            wert = wert + 1;
            return false;
        }
    }
    
    public String toString() {
        String string;
        
        if (wert <= 9) {
            string = "0" + wert;
        } else {
            string = "" + wert;
        }
        
        return string;
    }
    
    public void setzeWert(int neuerWert) {
        if (neuerWert >= limit) {
            System.out.println("Fehler: " + neuerWert + " zu groß!");
            // Programm muss abgebrochen werden.
        } else {
            wert = neuerWert;
        }
    }
}

// 1. Minuten/Stunden Überläufe
// 2. ZeitAnzeige.gibWert(), die "wert" zurückgibt
// 3. String zeitAusgabe soll immer aktuell sein
// 4. Man soll die Uhr mit unterschiedlichen 
//    Uhrzeiten belegen können
