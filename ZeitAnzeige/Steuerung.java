public class Steuerung {
    private ZeitAnzeige stundenAnzeige;    
    private ZeitAnzeige minutenAnzeige;    
    private ZeitAnzeige sekundenAnzeige;
    private String      zeitAusgabe;

    public Steuerung() {
        stundenAnzeige  = new ZeitAnzeige(24);
        minutenAnzeige  = new ZeitAnzeige(60);
        sekundenAnzeige = new ZeitAnzeige(60);
        zeitFormatieren();
    }

    public void zeitFormatieren() {
        // Sammel die drei Strings der Anzeigen zusammen
        // und trenne sie mit Doppelpunkt;
        zeitAusgabe = "";
        zeitAusgabe = zeitAusgabe + stundenAnzeige.toString();
        zeitAusgabe = zeitAusgabe + ":" + minutenAnzeige.toString();
        zeitAusgabe = zeitAusgabe + ":" + sekundenAnzeige.toString();
    }
    
    public void tick() {
        boolean ueberlauf;

        ueberlauf = sekundenAnzeige.tick();
        if (ueberlauf) {
            ueberlauf = minutenAnzeige.tick();
            if (ueberlauf) {
                stundenAnzeige.tick();
            }
        }

        zeitFormatieren();
    }
    
    public void setzeZeit(int neueStunden, int neueMinuten, int neueSekunden) {
        sekundenAnzeige.setzeWert(neueSekunden);
        minutenAnzeige.setzeWert(neueMinuten);
        stundenAnzeige.setzeWert(neueStunden);
        zeitFormatieren();
    }
}
