class Lift {
  int oberstesStockwerk;
  int unterstesStockwerk;
  int maxPersonen, anzahlPersonen;
  int stock, zielStock;
  boolean tuerZu;
  
  public Lift() {
    oberstesStockwerk = 5;
    unterstesStockwerk = -2;
    maxPersonen = 3;
    anzahlPersonen = 0;
    stock = 0;
    zielStock = 0;
    tuerZu = true;
  }
  
  // Methoden
  public void tuerSchliessen() {
    tuerZu = true;
  }
  
  public void tuerOeffnen() {
    tuerZu = false;
  }

  // Hausaufgabe: Meldung ausgeben, wie viele draußen bleiben mussten
  // bzw. dass keiner einsteigen darf
  public void einsteigen(int neuePersonen) {
    if (tuerZu) {
      System.out.println("Die Tuer ist zu!");
    } else {
      if (anzahlPersonen + neuePersonen > maxPersonen) {
        System.out.println("Es duerfen keine weiteren Personen einsteigen!");
        anzahlPersonen = maxPersonen;
      } else  {
        anzahlPersonen = anzahlPersonen + neuePersonen;
      }
    }
  }
 
  public void aussteigen(int anzahl) {
    // Hausaufgabe
  }
 
  public void setzeZielStockwerk(int neuerZielStock) {
    // Fehlt: Überprüfung, ob neuerZielStock gültig ist!
    zielStock = neuerZielStock;
  }
   
  public void fahre() {
    // Überprüfe, ob Tür zu ist, sonst Fehlermeldung
    // Überprüfe, ob er nach unten oder oben fahren soll
    // Wenn nach unten, dann verringere stock um 1
    // Wenn nach oben, dann erhöhe stock um 1
    // Wenn Ziel erreicht, öffne Tür
  }
  
}