public class Bank {
  private int   anzahlKonten;
  private Konto maikesKonto;
  private Konto antonsKonto;
  
  public Bank() {
    anzahlKonten = 2;
    maikesKonto  = new Konto(1, "Maike");
    antonsKonto  = new Konto(2, "Anton");

    ueberweisen(antonsKonto, maikesKonto, 200);
  }
  
  public void ueberweisen(Konto quellKonto, Konto zielKonto, float betrag) {
      boolean erfolgreich = quellKonto.abheben(betrag);
      String q = quellKonto.gibKontoinhaber();
      String z = zielKonto.gibKontoinhaber();

      if (erfolgreich == true) {
          zielKonto.einzahlen(betrag);
          System.out.println(q + " hat " + z + betrag + "€ überw.");
      } else {
          System.out.println("Konnte nicht überweisen.");
      }
  }
}

// Hausaufgaben
// 1. Statusmeldung: "Maike hat Anton 200€ überwiesen"
// 2. In der Klasse Konto: alle Meldungen so ändern, dass immer Kontonummer und Inhaber ausgegeben werden.
// 3. Was sind mögliche Fehler?

// Programmiere ALLE (!!!!) Getter_Methoden
