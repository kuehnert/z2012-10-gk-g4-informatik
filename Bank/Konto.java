public class Konto {
    private float dispokredit;
    private String kontoinhaber;
    private int kontonummer;
    private float kontostand;

    public Konto(int neueNr, String neuerInhaber) {
        dispokredit = 0.0f;
        kontostand = 0.0f;
        kontoinhaber = neuerInhaber;
        kontonummer = neueNr;
    }

    public void einzahlen(float betrag) {
        if (betrag <= 0) {
            System.out.println("Einzuzahlender Betrag muss positiv sein.");
        } else {
            kontostand = kontostand + betrag;
            System.out.println(betrag + " € eingezahlt.");
        }
    }

    public boolean abheben(float betrag) {
        boolean erfolgreich;

        if (betrag <= 0) {
            System.out.println("Abzuhebender Betrag muss größer 0 sein.");
            erfolgreich = false;
        } else if (kontostand + dispokredit < betrag) {
            System.out.println("Das Konto ist für diese Überweisung nicht gedeckt.");
            erfolgreich = false;
        } else {
            System.out.println(betrag + " € abgehoben.");
            kontostand = kontostand - betrag;
            erfolgreich = true;
        }
        
        return erfolgreich;
    }

    // public void ueberweisen(ZIELKONTO, float betrag) {
    // }

    public void kontoAuszug() {
        System.out.println("---------------------------");
        System.out.println("| Der Konstostand beträgt |" + kontostand + " €");
        System.out.println("---------------------------");
    }

    public float gibDispokredit() {
        return dispokredit;
    }

    public void setzeDispokredit(float neuerDispokredit) {
        if (-kontostand > neuerDispokredit) {
            System.out.println("Das Konto wäre bei dem Dispo nicht gedeckt.");
        } else {
            dispokredit = neuerDispokredit;
        }
    }

    public String gibKontoinhaber() {
        return kontoinhaber;
    }

    public int gibKontonummer() {
        return kontonummer;
    }

    public float gibKontostand() {
        return kontostand;
    }
    
    public void setzeKontoinhaber(String neuerKontoinhaber) {
        kontoinhaber = neuerKontoinhaber;
    }
}
