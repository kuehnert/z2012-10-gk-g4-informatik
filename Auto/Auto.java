// Klassendefinition Auto
public class Auto {
    // Attribute als
    // private Instanzvariablen
    // werden hier
    // deklariert (Deklaration)
    // <Datentyp> <Variablenname>
    int maxGeschwindigkeit;
    int sitzplaetze;
    boolean licht;
    float laenge;
    float verbrauch;
    String farbe;
    String kennzeichen;

    // Konstruktor
    public Auto() {
        System.out.println("Neues Auto wird erzeugt.");
      
        // Initialisierung = Erstbelegung, -Zuweisung
        // <Variablenname> = <Wert> / <Ausdruck>
        maxGeschwindigkeit = 100;
        sitzplaetze = 5;
        laenge      = 4.0f;
        verbrauch   = 40.5f;
        farbe       = "rot";
        kennzeichen = "Unbekannt";
        licht       = false;
    }

    // Methoden
    // <public|private> <void|Rückgabetyp> <Methodenname>() { Anweisungsblock }
    
    public void hupen() {
        System.out.println("Huuuuuup!");
    }

    public void lichtEinschalten() {
        if (licht == true) {
            System.out.println("Licht ist schon an!");
        } else {
            System.out.println("Licht eingeschaltet.");
            licht = true;
        }

    }
    
    public void lichtAuschalten() {
        if (licht == false) {
            System.out.println("Licht ist doch schon aus!");
        } else {
            System.out.println("Licht ausgeschaltet.");
            licht = false;
        }
    }

    public void fahren() {
        System.out.println("Brumm, brumm");
    }

    public void kofferraumOeffnen() {
        System.out.println("Klonk.");
    }
    
    public void status() {
        gibKennzeichen();
        gibMaxGeschwindigkeit();
        gibFarbe();
        gibAnzahlSitzplaetze();
        gibLaenge();
        gibVerbrauch();
        gibLicht();
        System.out.println();
    }

    // getter-Methode 
    public void gibKennzeichen() {
        System.out.println("Kennzeichen: " + kennzeichen);
    }
    
    public void gibMaxGeschwindigkeit() {
        System.out.println("Maximale Geschwindigkeit: " + maxGeschwindigkeit);
        // ("Einfacher Text[String]" + <Variable>)
    }
    
    public void gibAnzahlSitzplaetze() {
        System.out.println("Anzahl der Sitzplaetze: " + sitzplaetze);
    }
    
    public void gibLaenge() {
        System.out.println("Laenge des Wagens: " + laenge);
    }
    
    public void gibLicht() {
        System.out.print("Licht: ");
        if (licht == false) {
            System.out.println("aus");
        } else {
            System.out.println("an");
        }
    }
    
    public void gibVerbrauch() {
        System.out.println("Spritverbrauch: " + verbrauch);
    }
    
    public void gibFarbe() {
        System.out.println("Farbe des Wagens: " + farbe);
    }
    
   
    // setter-Methode       | Parameter
    //                      |
    //                      V
    public void setzeFarbe(String neue_farbe) {
        farbe = neue_farbe;
    }  
    
    public void setzeKennzeichen(String neuesKz) {
        kennzeichen = neuesKz;
    }
    
    
    public void setzeAnzahlSitzplaetze(int anzahl_sitzplatze) {
        sitzplaetze = anzahl_sitzplatze;
    }
    
    public void setzeMaximaleGeschwindigkeit(int max_geschwindigkeit) {
        maxGeschwindigkeit = max_geschwindigkeit; 
    } 
   
    public void setzeLaenge(float neue_laenge) {
        laenge = neue_laenge;
    }
   
    public void setzeVerbrauch(float neuer_verbrauch) {
        verbrauch = neuer_verbrauch;
    }
}

// Auto mein_auto = new Auto();
// mein_auto.lichtEinschalten()
// mein_auto.licht
// mein_auto.setzeFarbe("gruen");
