class IfBedingung {
  public IfBedingung() {
    // ifBedingung2();
  }
  
  public void ifBedingung1() {
    int alter = 24;
    
    System.out.println("Du darfst:");
    
    if (alter >= 15) {
      System.out.println("Mofa fahren");
    }
    
    if (alter >= 16) {
      System.out.println("Bier kaufen");
    } 
    
    if (alter >= 18) {
      System.out.println("Schnaps kaufen");
    }
  }
  
public void ifBedingung2() {
  int wuerfel = (int) (Math.random() * 6) + 1;
  System.out.print(wuerfel);
    
  if (wuerfel == 1) {
    System.out.println("vorziehen");
  } else if (wuerfel == 2) {
    System.out.println("aussetzen");
  } else if (wuerfel == 3) {
    System.out.println("Super 3");
  } else if (wuerfel == 4) {
    System.out.println("4");
  } else if (wuerfel == 5) {
    System.out.println("5");
  } else {
    System.out.println("Tanz!");
  }
}
  
  public void ifBedingung3() {
    // Für ein bestimmtes Alter wird
    // ausgegeben, wie alt man "gefühlt"
    // ist. Beispiel:
    // 0-7: Baby
    // 8-14: nervig
    // 15-20: cool
    // 21-29: älter
    // 30-39: alt
    // 40-60: Oma und Opa
    // >60: uralt
    int alter = 55;
    
  }
  
public void zufallsZahlenWerten() {
  int einsen = 0;
  int zweien = 0;
  int dreien = 0;
  int vieren = 0;
  int fuenfen = 0;
  int sechsen = 0;
        
  for (int i = 0; i < 1200; i++) {
    double zd = Math.random() * 6;
    int zi = (int) zd + 1;
    // System.out.println("Gewuerfelt: " + zi);
    if (zi == 1) {
      einsen = einsen + 1;
    } else if (zi == 2) {
      zweien += 1;
    } else if (zi == 3) {
      dreien++;
    } else if (zi == 4) {
      vieren = vieren + 1;
    } else if (zi == 5) {
      fuenfen = fuenfen + 1;
    } else {
      sechsen = sechsen + 1;
    }
  }
    
  // System.out.println("1: " + einsen + "-mal");
  // System.out.println("2: " + zweien + "-mal");
  // System.out.println("3: " + dreien + "-mal");
  // System.out.println("4: " + vieren + "-mal");
  // System.out.println("5: " + fuenfen + "-mal");
  // System.out.println("6: " + sechsen + "-mal"); 

System.out.println("1: " + (double) einsen / 1200);
System.out.println("2: " + zweien / 1200.0);
System.out.println("3: " + dreien / 1200);
System.out.println("4: " + vieren / 1200);
System.out.println("5: " + fuenfen/ 1200);
System.out.println("6: " + sechsen/ 1200); 
}

  public static void main(String[] hanswurst) {
    new IfBedingung();
  }
}
