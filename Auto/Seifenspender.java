class Seifenspender {
    int kapazitaet;
    int fuellmenge;
    int gesamtmenge;

    public Seifenspender(int neueFuellmenge) {
        kapazitaet = 250;
        gesamtmenge = 0;

        if (neueFuellmenge >= kapazitaet) {
            fuellmenge = kapazitaet;
        } else if (neueFuellmenge < 0) {
            fuellmenge = 0;
        } else {
            fuellmenge = neueFuellmenge;
        }

    }

    public void spendeSeife() {
        // soll 30ml Seife ausgeben
        if (fuellmenge >= 30) {
            fuellmenge = fuellmenge - 30;
            gesamtmenge = gesamtmenge + 30;
            System.out.println("Blublublublub...");
        } else {
            // fuellmenge < 30
            System.out.println("Keine Seife da. Bitte nachfuellen!");
        }
    }

    public void nachfuellen(int nachfuellMenge) {
        if (nachfuellMenge < 0) {
            System.out.println("Witzbold. Du kannst keine Seife entnehmen beim Nachfuellen.");
        } else if (fuellmenge + nachfuellMenge <= kapazitaet) {
            fuellmenge = fuellmenge + nachfuellMenge;
            System.out.println("Gratuliere, Sie haben jetzt " + fuellmenge + "ml Seife.");
        } else {
            System.out.println("Das ist zu viel.");
        }
    }

    public void gibFuellmenge() {
        System.out.println("Die Fuellmenge ist " + fuellmenge);
    }

    public void gibGesamtmenge() {
        System.out.println("Die Gesamtmenge ist " + gesamtmenge);
    }
    
    public static void main(String[] args) {
        new Seifenspender(250);
    }
}
