import java.util.Random;

public class IfBedingung2 {
    public void bedingungen1(int alter) {
        // 12 vorne sitzen
        if (alter >= 12) {
          System.out.println("Vorne sitzen");
        }
        
        // 14 eingeschränkt geschäftsfähig
        if (alter >= 14) {
          System.out.println("geschäftsfähig");
        }

        // 15 Mofa fahren
        if (alter >= 15) {
          System.out.println("Mofa fahren");
        }

        // 16 Roller fahren
        if (alter >= 16) {
          System.out.println("Roller fahren");
        }
        
        // 17 Bier trinken
        if (alter >= 17) {
          System.out.println("Bier trinken");
        }
        
        // 18 Auto fahren
        if (alter >= 18) {
          System.out.println("Auto fahren");
        }
    }
    
    public void bedingungen2() {
      // Es soll gewürfelt werden
      // Je nach gewürfelter Augen
      // passieren unterschiedliche Dinge.
      
      // 1: Du gehst 2 Felder vor
      // 2: Du musst einmal aussetzen
      // 3: Die Spielrichtung wechselt
      // 4: Du musst ein Feld zurückgehen
      // 5: Nimm eine Ereigniskarte
      // 6: Du darfst nochmal würfeln
      
      // 1. Generator erzeugen
      Random generator = new Random();
      // 2. Generator Zahlen erzeugen lassen
      int zahl = generator.nextInt(6) + 1;
      
      System.out.println(zahl);
      
      // Zufallszahl zwischen 0 und 100
      // Zufallszahl zwischen 40 und 100
    }
    
}
