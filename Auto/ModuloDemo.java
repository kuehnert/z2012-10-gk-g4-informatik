import java.util.Random;

public class ModuloDemo {
  public void modulo_demo() {
    // c = a % b (Modulo)

    System.out.println( 24 %   6); // 0
    System.out.println( 23 %   6); // 5
    System.out.println( 25 %   5); // 0
    System.out.println( 24 %  24); // 0
    System.out.println(  6 % 100); // 6
    System.out.println(100 %   6); // 4
  }
  
  public void zufallszahl() {
    Random generator = new Random();

    for (int i = 0; i < 20; i++) {
      int zuffi = generator.nextInt();
      zuffi = zuffi % 6;
    
      if (zuffi < 0) {
          zuffi = -zuffi;
      }
      
      zuffi = zuffi + 1;
      
      System.out.println( zuffi );
    }
  }
  
  public static void main(String[] args) {
    new ModuloDemo();
  }
}
