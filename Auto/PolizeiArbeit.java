public class PolizeiArbeit {
    int hoechstgeschwindigkeit = 70;
    
    public void geschwindigkeitskontrolle(int kmh) {
      System.out.println("Polizeikontrolle");
      
      if (kmh > 90) { 
        System.out.println("Gefaengnis");
      } else if (kmh > 85) {
        System.out.println("30 Euro");
      } else if (kmh > 80) {
        System.out.println("20 Euro");
      } else if (kmh > 70) {
        System.out.println("10 Euro");
      } else {
        System.out.println("Super gefahren!");
      }
    }
}
