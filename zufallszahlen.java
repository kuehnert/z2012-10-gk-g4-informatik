// ========================
// = Zufallszahl zw. 0..1 =
// ========================
Math.random(); 0 <= x < 1

// ===========================
// = Zufallszahl zw. 0 und n =
// ===========================
# obere Grenze n = 30
int r = (int) (Math.random() * (n+1))
int r = (int) (Math.random() * 31)

// =================================
// = 3 mögliche Zufallszahlen ab 5 =
// =================================
// untere Grenze a = 5
// Anzahl möglicher Zahlen x = 3
int r = (int) (Math.random() * 3) + 5
int r = (int) (Math.random() * x) + a

// ===========================
// = Zufallszahl zw. a und b =
// ===========================
# untere Grenze a = 5
# obere  Grenze b = 7
int r = (int) (Math.random() * (3)) + 5
int r = (int) (Math.random() * (7-5+1)) + 5
int r = (int) (Math.random() * (b-a+1)) + a

public int zufallszahl(int a, b) {
  int r = (int) (Math.random() * (b-a+1)) + a
  System.out.println(r);
}
