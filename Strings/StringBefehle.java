public class StringBefehle {
    public StringBefehle() {
    }
    
    public void einfacheDinge() {
        // Statischer, literaler String (String Literals)
        System.out.println("Hallo Welt.");
        
        // speichern als Datentyp
        String meinString = "Hallo Welt";
        System.out.println( meinString );
        
        // Steuerzeichen
        String a = "Zeilenumbruch\nNeue Zeile";
        System.out.println(a);
        String b = "Tabu\tlator";
        System.out.println(b);
        
        // Konkatenation 
        String c = "A";
        String d = "B";
        String e = c + d; // => "AB"
        e = c.concat(d); // => "AB"
        
        // Umwandeln von anderen Datentypen in Strings
        int i = -10;
        String f = Integer.toString(i);
        System.out.println(f);

        // Umwandeln von Strings in andere Datentypen
        String g = "-5";
        int j = Integer.parseInt(g);
        System.out.println(j);
        // Bei ungültigen Zahlen (in Stringform) wird das Programm abgebrochen
        // String h = "Hallo-5";
        // int k = Integer.parseInt(h);
        // System.out.println(k);
        
        // Vergleichen von Zeichenketten
        // 1. Falsch
        a = "Test";
        b = "test";
        c = "Test";
        d = "Te";
        e = d + "st";
        System.out.println( a == b ); // false
        System.out.println( a == c ); // true
        System.out.println( e );
        System.out.println( a == e ); // false!
        
        // 2. Richtig
        System.out.println( a.equals(b) ); // false 
        System.out.println( a.equals(c) ); // true
        System.out.println( a.equals(e) ); // true
        System.out.println( a.equalsIgnoreCase(b) );
        // => true
    }
    
    public void zeichenFuerZeichen() {
        String s = "FuchsDuHastDieGansGestohlen!";
        char c = s.charAt(0); // Liefert 0. Zeichen
        System.out.println(c); // => "F"
        c = s.charAt(1); // Liefert 1. Zeichen
        System.out.println(c); // => "u"
        c = s.charAt(2); // Liefert 2. Zeichen
        System.out.println(c); // => "c"
        
        int laenge = s.length();
        for (int i = 0; i < laenge; i = i + 1) {
            c = s.charAt(i);
            System.out.println( c );
        }
        
        System.out.println( laenge );
        System.out.println( s.charAt(200) );
    }
    
    public boolean meinEquals(String a, String b) {
        // Wenn die Strings unterschiedlich lang sind,
        // gib false zurück.
        if ( a.length() != b.length() ) {
            return false;
        }
        
        // Man geht von vorne nach hinten durch den String,
        // Dann vergleicht man Zeichen für Zeichen die beiden Strings
        int laenge = a.length();
        for (int i = 0; i < laenge; i = i + 1) {
            char charA = a.charAt(i);
            char charB = b.charAt(i);
            
            // Wenn sich die Zeichen an der gleichen Stelle unterscheiden,
            // brich ab und gib "false" zurück.
            if (charA != charB) {
                return false;
            }
        }
        
        // Wenn er durch den ganzen String durch ist (keine Unterschiede
        // festgestellt hat), gib "true" zurück.
        return true;
    }
    
    public boolean meinEqualsIgnoreCase(String a, String b) {
        // a = "HaLLo+1"
        // b = "haLlO+1" => true
        // a.toUpperCase() => "HALLO+1"
        // b.toUpperCase() => "HALLO+1"

        a = a.toUpperCase();
        b = b.toUpperCase();
        boolean gleich = meinEquals(a, b);
        
        return gleich;
    }
    
    public void testeMeinEquals() {
        // Vergleichen von Zeichenketten
        String a = "Test";
        String b = "test";
        String c = "Test";
        String d = "Te";
        String e = d + "st";
        System.out.println( meinEquals(a, b) ); // false 
        System.out.println( meinEquals(a, c) ); // true
        System.out.println( meinEquals(a, e) ); // true
        // System.out.println( meinEqualsIgnoreCase(a, b) ); // => true
    }
    
    public void testeMeinEqualsIgnoreCase() {
        // Vergleichen von Zeichenketten
        String a = "HaLLö+1";
        String b = "haLlÖ+1";
        String c = "hall0+1";
        String d = "";
        String e = "HaLLo+12";
        System.out.println( meinEqualsIgnoreCase(a, b) ); // true
        System.out.println( meinEqualsIgnoreCase(a, c) ); // false
        System.out.println( meinEqualsIgnoreCase(a, d) ); // false
        System.out.println( meinEqualsIgnoreCase(a, e) ); // false
    }
    
    public static void main(String[] args) {
        StringBefehle programm = new StringBefehle();
        // programm.zeichenFuerZeichen();
        programm.testeMeinEqualsIgnoreCase();
        
        System.out.println( "ä".toUpperCase() );
        System.out.println( "ö".toUpperCase() );
        System.out.println( "é".toUpperCase() );
        System.out.println( "ß".toUpperCase() );
        // String s = "String";
        // System.out.println(s.toUpperCase());
        // System.out.println(s.toLowerCase());
    }
}
