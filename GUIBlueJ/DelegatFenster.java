// Import-Anweisung für unseren JFrame
import javax.swing.JFrame;
import javax.swing.JButton;
 
public class DelegatFenster {
    public static void main(String[] args) {
        /* Erzeugung eines neuen Frames mit dem 
           Titel "Beispiel JFrame " */       
        JFrame meinFrame = new JFrame("Beispiel JFrame");
        meinFrame.setSize(800, 600);
        
        JButton okButton = new JButton("OK");
        okButton.setSize(100, 40);
        JButton abbrechenButton = new JButton("Abbrechen");
        abbrechenButton.setSize(100, 40);
        
        meinFrame.add( okButton );
        meinFrame.add( abbrechenButton );
        
        meinFrame.setVisible(true);
    }
}
