public class ZahlenPruefer {
    /*
     * 234324234432.32167836782167836127836712637868
     * <-Ganzzahl->.<- 2. Ganzzahl ---------------->
     * 
     * 1. Methode istGueltigeGanzzahl
     * 2. "hsjh".subString(int startPosition, int laenge)
     * 
     */
    
    public static boolean istGueltigeGanzzahl(String eingabe) {
        for (int i = 0; i < eingabe.length(); i++) {
            char c = eingabe.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        
        return true;
    }

    public static boolean istGueltigeKommazahl(String eingabe) {
        return false;
    }
    
    public static void main(String[] args) {
        System.out.println( istGueltigeGanzzahl("1") );
        System.out.println( istGueltigeGanzzahl("0") );
        System.out.println( istGueltigeGanzzahl("A") );
        System.out.println( istGueltigeGanzzahl("01A") );
        System.out.println( istGueltigeGanzzahl("B1") );
        System.out.println( istGueltigeGanzzahl("1423784237847328") );
    }
}
