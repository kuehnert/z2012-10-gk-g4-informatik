import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MeinFenster extends JFrame {
    JPanel     myPanel         = new JPanel();
    JButton    okButton        = new JButton("OK");
    JButton    abbrechenButton = new JButton("Abbrechen");
    JTextField tf              = new JTextField("", 20);

    class MyButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == okButton) {
                tf.setText( "OK gedrueckt" );
            } else {
                tf.setText( "Abbrechen gedruckewt" );
            }
        }
    }
    
    public MeinFenster() {
        super("Mein Super-Fenster!");
        setSize(640, 400);

        myPanel.setLayout( new BoxLayout(myPanel, BoxLayout.Y_AXIS) );
        okButton.addActionListener( new MyButtonListener() );
        abbrechenButton.addActionListener( new MyButtonListener() );
        
        myPanel.add( okButton );
        myPanel.add( abbrechenButton );
        myPanel.add( tf );
        add(myPanel);

        setVisible(true);
    }
}
