import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Waehrung extends JFrame {
    JPanel     myPanel         = new JPanel();
    JButton    rechnenButton   = new JButton("Umrechnen");
    JLabel     kursLabel          = new JLabel("Kurs:");
    JLabel     euroLabel          = new JLabel("Betrag in €:");
    JLabel     zielLabel          = new JLabel("Fremdwährung:");
    JTextField kursFeld        = new JTextField("1.31111", 8);
    JTextField euroFeld        = new JTextField("100", 8);
    JTextField zielFeld        = new JTextField("-", 8);
        
    class MyButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String s = kursFeld.getText();
            double kurs = Double.parseDouble(s);

            s = euroFeld.getText();
            if (istGueltigeZahl(s)) {
                double euro = Double.parseDouble(s);
                double fremd = euro * kurs;
                zielFeld.setText("" + fremd);
            } else {
                zielFeld.setText("Ungültiger Wert!");
            }
            
        }
    }
    
    public Waehrung() {
        super("Mein Währungsumrechner!");
        setSize(640, 400);

        myPanel.setLayout( new BoxLayout(myPanel, BoxLayout.Y_AXIS) );
        rechnenButton.addActionListener( new MyButtonListener() );
        
        zielFeld.setEditable(false);
        
        myPanel.add( euroLabel );
        myPanel.add( euroFeld );
        myPanel.add( kursLabel );
        myPanel.add( kursFeld );
        myPanel.add( rechnenButton );
        myPanel.add( zielLabel );
        myPanel.add( zielFeld );
        add(myPanel);

        setVisible(true);
    }
    
    private static boolean istGueltigeZahl(String s) {
        return false;
    }
    
    public static void main(String[] a) {
        new Waehrung();
    }
}
