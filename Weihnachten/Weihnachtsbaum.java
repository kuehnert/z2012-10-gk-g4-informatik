public class Weihnachtsbaum {
    public Weihnachtsbaum() {
        baum1();
    }
    
    public void leerzeichen(int anzahl) {
        int i = 0;
        while (i < anzahl) {
            System.out.print(" ");
            i = i + 1;
        }
    }

    public void rauten(int anzahl) {
        for (int i = 0; i < anzahl; i = i + 1) {
             System.out.print("#");
        }
    }

    public void baum1() {
        int i = 0;
        while (i < 4) {
            rauten(i * 2 + 1);
            System.out.println();
        }
    }
    
    public static void main(String[] args) {
        new Weihnachtsbaum();
    }
}
 