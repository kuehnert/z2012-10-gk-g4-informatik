class Monopoly {
	int[] werte = { 500, 200, 100, 50, 20, 10, 5, 2, 1 };

	public String stueckelung(int betrag) {
		int register = 0;
		String ausgabe = "";
		
		while (betrag > 0) {
			if (betrag >= werte[register]) {
				ausgabe = ausgabe + Integer.toString(werte[register]) + "€ ";
				betrag = betrag - werte[register];
			} else {
				register = register + 1;
			}
		}
		
		return ausgabe;
	}
	
	public Monopoly() {
		System.out.println( stueckelung(  19) );
		System.out.println( stueckelung(  43) );
		System.out.println( stueckelung(  75) );
		System.out.println( stueckelung(  98) );
		System.out.println( stueckelung( 149) );
		System.out.println( stueckelung(3998) );
	}
	
	public static void main(String[] args) {
		new Monopoly();
	}
}
