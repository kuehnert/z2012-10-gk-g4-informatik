class Suchen {
	public Suchen() {
		int[] feld;
		feld = new int[]{ 4, -7, 2, 9, 4, 6, 5, 100 };
		int[] feld2 = { 20, 20, 9, 6, 5 };
		int[] feld3 = { 2, 1, 20, 20, 9, 5 };

		// System.out.println( suchen(feld, 4) );
		// System.out.println( suchen(feld, 7) );
		// System.out.println( suchen(feld, 2) );
		// System.out.println( suchen(feld, 9) );
		// System.out.println( suchen(feld, 5) );
		// System.out.println( suchen(feld, 888) );
		// System.out.println( suchen(feld, -1) );
		
		System.out.println( groesste(feld) );
		System.out.println( zweitgroesste(feld) );
		System.out.println( zweitgroesste(feld2) );
		System.out.println( zweitgroesste(feld3) );
	}
	
	// Gibt die Stelle in <feld> zurück, an der
	// das erste Mal der Wert <zahl> steht.
	// Ist <zahl> nicht enthalten, gib -1 zurück
	public int suchen(int[] feld, int zahl) {
		for (int i = 0; i < feld.length; i = i + 1) {
			if (feld[i] == zahl) {
				return i;
			}
		}
		
		return -1;
	}

	// groesste Zahl
	public int groesste(int[] feld) {
		int max = feld[0];
			
		for (int i = 1; i < feld.length; i = i + 1) {
			if (feld[i] > max) {
				max = feld[i];
			}
		}
		
		return max;
	}
	
	// ZWEIT-groesste Zahl
	public int zweitgroesste(int[] feld) {
		int max  = feld[0];
		int max2 = feld[1];

		if (feld[1] > feld[0]) {
			max  = feld[1];
			max2 = feld[0];
		}
			
		for (int i = 2; i < feld.length; i = i + 1) {
			if (feld[i] > max) {
				max2 = max;
				max = feld[i];
			} else if (feld[i] > max2 && feld[i] != max) {
				max2 = feld[i];
			}
		}
		
		return max2;
	}
	
	public static void main(String[] args) {
		new Suchen();
	}
}