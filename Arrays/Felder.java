class Felder {
	public Felder() {
		ausgabe5();
		
		int[] a = {};
		feld_ausgeben( a );
		
		int[] b = { 0 };
		feld_ausgeben( b );
	}
	
	public void feld_erzeugen() {
		int[] feld = new int[5];
		
		for (int i = 0; i < feld.length; i = i + 1) {
			feld[i] = 1;
		}
		
		feld_ausgeben(feld);
	}
	
	public void ausgabe1() {
		// {0, 1, 2, 3, 4}
		int[] feld = new int[5];
		
		for (int i = 0; i < feld.length; i = i + 1) {
			feld[i] = i;
		}
		
		feld_ausgeben(feld);
	}

	public void ausgabe2() {
		// {1, 2, 3, 4, 5, ....}
		int[] feld = new int[5];
		
		for (int i = 0; i < feld.length; i = i + 1) {
			feld[i] = i + 1;
		}
		
		feld_ausgeben(feld);
	}
	
	public void ausgabe3() {
		// {2, 4, 6, 8, 10, 12, 14, ...}
		int[] feld = new int[5];
		
		for (int i = 0; i < feld.length; i = i + 1) {
			feld[i] = (i + 1) * 2;
		}
		
		feld_ausgeben(feld);
	}

	public void ausgabe4() {
		// {0, 1, 4, 9, 16, 25, ...}
		int[] feld = new int[20];
		
		for (int i = 0; i < feld.length; i = i + 1) {
			feld[i] = (int) Math.pow(i, 2);
		}
		
		feld_ausgeben(feld);
	}

	// Vieren filtern
	// {1, 2, 3, 5, 6, 7, 9, 10, 11, 13, ...}
	public void ausgabe5() {
		int[] feld = new int[20];
		int z = 1;
		
		for (int i = 0; i < feld.length; i = i + 1) {
			if (z % 4 == 0) {
				z = z + 1;
				feld[i] = z;
			} else {
				feld[i] = z;
			}
			
			z = z + 1;
		}
		
		feld_ausgeben(feld);
	}
	
	// {1, 2, -1, 4, 5, -1, }
	public void ausgabe6() {
		int[] feld = new int[20];
		
		for (int i = 0; i < feld.length; i = i + 1) {
			if ((i + 1) % 3 == 0) {
				feld[i] = -1;
			} else {
				feld[i] = i + 1;
			}
			
		}
		
		feld_ausgeben(feld);
	}
	
	public void ausgabe7() {
		// {4, 3, 2, 1, 0}
		int[] feld = new int[20];
		
		for (int i = 0; i < feld.length; i = i + 1) {
			feld[i] = feld.length - i - 1;
		}
		
		feld_ausgeben(feld);
	}
	
	
	// "2 4 6 8 10 " => "{ 2, 4, 6, 8, 10 }"
	public void feld_ausgeben(int[] a) {
		if (a.length > 0) {
			String out = "{ " + a[0];
		
			for (int i = 1; i < a.length; i = i + 1) {
				out += ", " + a[i];
			}
			out += " }";

			System.out.println( out );
		} else {
			System.out.println( "{}");
		}
	}
	
	
	public static void main(String[] args) {
		new Felder();
	}
}