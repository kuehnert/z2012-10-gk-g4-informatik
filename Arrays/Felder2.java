import java.util.*;

class Felder2 {
	// Es gibt ein Feld a, das x Elemente enthält, die größer sind als 0. Nach dem x. Element, sind zum Füllen lauter 0en gespeichert.
	// Aufgabe: Erzeuge ein neues Feld b, welches die Zahlen aus a enthält, aber die richtige Größe hat
	// a = {1,2,3,0,0,0,0,0,0}
	// => b = {1, 2, 3}
	public int[] quetschen(int[] a) {
		// Suche die 1. 0 und speichere ihre Position in laenge
		// int laenge = 0;
		// for (int i = 0; i < a.length; i++) {
		// 	if ( a[i] == 0) {
		// 		laenge = i;
		// 		break;
		// 	}
		// }

		int laenge = 0;
		while (laenge < a.length && a[laenge] != 0) {
			laenge++;
		}
		
		// Erzeuge ein neues Feld b mit der richtigen Länge
		int[] b = new int[laenge];
		
		// Übertrage einzeln die Elementen von a in b
		for (int i = 0; i < laenge; i++) {
			b[i] = a[i];
		}
		
		return b;
	}

	// Finde alle Quadratzahlen, die zwischen 2000 und 5000 liegen, und speichere sie in einem Feld
	public int[] quadratzahlen() {
		int min = 2000;
		int max = 5000;
		
		int[] zahlen = new int[ max - min ];
		int z = (int) Math.sqrt(min); 
		int y = 0;
		int quadrat;
		while ((quadrat = z * z) <= max) {
			zahlen[y] = quadrat;
			y++;
			z++;
		}
		
		return zahlen;
	}
		
	public Felder2() {
		// int[] a = {1,2,3,0,0,0,0,0,0};
		// int[] c = quetschen(a);
		// feld_ausgeben(a);
		// feld_ausgeben(c);
		System.out.println( Arrays.toString(quadratzahlen() ) );
	}
	
	public static void main(String[] args) {
		new Felder2();
	}

	public void feld_ausgeben(int[] a) {
		if (a.length > 0) {
			String out = "{ " + a[0];
		
			for (int i = 1; i < a.length; i = i + 1) {
				out += ", " + a[i];
			}
			out += " }";

			System.out.println( out );
		} else {
			System.out.println( "{}");
		}
	}
}
