class RoemischeZahlen {
	public String roemisch(int betrag) {
		int[] werte      = { 1000, 500,  100,  50,  10,   5,   1 };
		String[] ziffern = {  "M",  "D", "C", "L", "X", "V", "I" };
		int register   = 0;
		String ausgabe = "";
		
		while (betrag > 0) {
			if (betrag >= werte[register]) {
 				ausgabe = ausgabe + ziffern[register];
				betrag  = betrag  - werte[register];
			} else {
				register = register + 1;
			}
		}
		
		return ausgabe;
	}

	public String roemischSubtraktion2(int betrag) {
		int[] werte      = { 1000, 500, 400, 100, 50, 40, 10,   5, 4,   1 };
		String[] ziffern = {  "M", "D", "CD", "C", "L", "XL", "X", "V", "IV", "I" };
		int register   = 0;
		String ausgabe = "";
		
		while (betrag > 0) {
			if (betrag >= werte[register]) {
 				ausgabe = ausgabe + ziffern[register];
				betrag  = betrag  - werte[register];
			} else {
				register = register + 1;
			}
		}
		
		return ausgabe;
	}
	
	public String roemischSubtraktion(int betrag) {
		String ausgabe = roemisch(betrag);
		
		ausgabe = ausgabe.replace("IIII", "IV");
		ausgabe = ausgabe.replace("XXXX", "XL");
		ausgabe = ausgabe.replace("CCCC", "CD");
		
		return ausgabe;
	}
	
	public RoemischeZahlen() {
		System.out.println( roemisch(  19) );
		System.out.println( roemisch(  43) );
		System.out.println( roemisch(  75) );
		System.out.println( roemisch(  98) );
		System.out.println( roemisch( 149) );
		System.out.println( roemisch(3998) );
		System.out.println();

		System.out.println( roemischSubtraktion(  19) );
		System.out.println( roemischSubtraktion(  43) );
		System.out.println( roemischSubtraktion(  75) );
		System.out.println( roemischSubtraktion(  98) );
		System.out.println( roemischSubtraktion( 149) );
		System.out.println( roemischSubtraktion(3998) );
		System.out.println();

		System.out.println( roemischSubtraktion2(  19) );
		System.out.println( roemischSubtraktion2(  43) );
		System.out.println( roemischSubtraktion2(  75) );
		System.out.println( roemischSubtraktion2(  98) );
		System.out.println( roemischSubtraktion2( 149) );
		System.out.println( roemischSubtraktion2(3998) );
		System.out.println();
	}
	
	public static void main(String[] args) {
		new RoemischeZahlen();
	}
	
}